<?php

define('ROOT_DIR', __DIR__); 
define('CORE_DIR', ROOT_DIR.'/core'); 
define('MDIR', ROOT_DIR.'/models'); 
define('VDIR', ROOT_DIR.'/views');
define('CDIR', ROOT_DIR.'/controllers');

// Require core classes and helpers

require ROOT_DIR.'/config.php';
require ROOT_DIR.'/vendor/autoload.php';

require CORE_DIR.'/app.php';
require CORE_DIR.'/view.php';
require CORE_DIR.'/controller.php';
require CORE_DIR.'/model.php';


// RUN App
$app = new app;

$app->run();