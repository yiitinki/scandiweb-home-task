<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home Tasks - ScandiWeb</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo View::assets("vendor/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo View::assets("css/4-col-portfolio.css"); ?>" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo URL; ?>">Home Tasks - ScandiWeb</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo URL."/product" ?>">Product List</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo URL."/product/create" ?>">Product Add</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
