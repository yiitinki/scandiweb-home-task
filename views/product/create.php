
<!-- Page Content -->
<div class="container">

  <!-- Page Heading -->
  <h1 class="my-4">Product Add
  </h1>

  <div id="alertBox" class="row" style="margin-top:5px" >

    <div id="error" style="display:none" class="alert alert-danger col align-self-center" role="alert"><ul></ul></div>

    <div id="success" style="display:none" class="alert alert-success col align-self-center" role="alert"><ul></ul></div>

  </div>

  <div class="row" >
    <div class="col-md-6">

    <form id="productAddForm">
        <div class="form-group">
            <label for="productSKU">SKU</label>
            <input type="text" name="sku" class="form-control" id="productSKU" placeholder="SKU">
        </div>

        <div class="form-group">
            <label for="productName">Name</label>
            <input type="text" name="name" class="form-control" id="productName" placeholder="Name">
        </div>

        <div class="form-group">
            <label for="productPrice">Price</label>
            <input type="text" name="price" class="form-control" id="productPrice" placeholder="Price">
        </div>

        <div class="form-group">
            <label for="typeSwitcher">Type Switcher</label>
            <select name="type" class="form-control" id="typeSwitcher">
                <?php foreach($types as $type){ ?>
                    <option value="<?php echo $type["type_id"] ?>"><?php echo $type["title"] ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="form-group" id="1">
            <label for="dvdSize">Size</label>
            <input type="text" name="size" class="form-control" id="dvdSize" placeholder="Size">
            <small class="form-text text-muted">Please determine the size of the DVD (in MB)</small>
        </div>

        <div class="form-group" id="2" style="display:none">
            <label for="bookWeight">Weight</label>
            <input type="text" name="weight" class="form-control" id="bookWeight" placeholder="Weight">
            <small class="form-text text-muted">Please determine the weight of the Book (in KG)</small>
        </div>

        <div class="form-group" id="3" style="display:none">
            <label for="FurnitureHeight">Height</label>
            <input type="text" name="height" class="form-control" id="FurnitureHeight" placeholder="Height">
            <label for="FurnitureWidht">Widht</label>
            <input type="text" name="widht" class="form-control" id="FurnitureWidht" placeholder="Widht">
            <label for="FurnitureLenght">Lenght</label>
            <input type="text" name="lenght" class="form-control" id="FurnitureLenght" placeholder="Lenght">
            <small class="form-text text-muted">Please determine the dimensions of the Furniture</small>
        </div>


        <div class="form-group"><button type="submit" id="submitPost" class="btn btn-primary">Submit</button></div>
    </form>

    </div>
    <div class="col-md-6"></div>
  </div>
  <!-- /.row -->



</div>
<!-- /.container -->

