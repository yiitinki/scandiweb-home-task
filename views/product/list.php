
<!-- Page Content -->
<div class="container">

  <!-- Page Heading -->
 <h1 class="my-4">Product List</h1> 
  
  <div id="alertBox" class="row" style="display:none">

    <div class="alert alert-danger col align-self-center" role="alert">
      Are you sure for delete selected products?<br>
      <button id="deleteYes" type="button" class="btn btn-danger btn-sm">Yes</button>
      <button id="deleteNo" type="button" class="btn btn-dark btn-sm">No</button>

    </div>

  </div>

  <div id="success" class="row" style="display:none">
  
    <div class="alert alert-success col align-self-center" role="alert">Selected products has been deleted successfully.</div>
  
  </div>


  <div id="deleteBox" class="row">
  
    <div class="col align-self-end"> <button id="delete" type="button" class="btn btn-danger float-right" style="margin-bottom:10px;" disabled>Delete</button> </div>

  </div>


  
  <div class="row">

  <!--
    There was another option to use database relationship beetween products and types table.
    When we consider "types" are static and each additional "type" need additional input on product add page.
    Because of it, I prefered code based solution rather than db based solution.
    -->

  <?php 
  foreach ($products as $field){
    ?>
  
    <div id="<?php echo $field["product_id"] ?>" class="col-lg-3 col-md-4 col-sm-6 portfolio-item">

      <div class="card h-100">
        <div class="form-check" style="margin-left:10px;margin-top:10px;">
          <input class="form-check-input position-static checkbox" name="checkbox[]" type="checkbox" id="blankCheckbox" value="<?php echo $field["product_id"] ?>">
        </div>
        <div class="card-body">

          <h4 class="card-title">
            <?php echo $field["name"] ?>
          </h4>
            <p class="card-text">
              <b>SKU:</b><?php echo $field["sku"] ?> <br>
              <b>Price:</b><?php echo $field["price"] ?> <br>
              <b>Type:</b><?php echo $field["title"]; ?> <br>
              <b><?php echo $field["description_title"]; ?>:</b><?php echo $field["description"] ?> <br>
            
            </p>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  <!-- /.row -->



</div>
<!-- /.container -->

