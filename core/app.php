<?php
class app
{
 
  public $controller, $action, $params;

  public function __construct()
  {
    
 
    // URL check
    if(isset($_GET['url']) AND !empty($_GET['url'])){

      $url = trim($_GET['url'], '/');

    }else{

      $url = 'default/index';
    
    }

    $url = explode('/', $url);
 
    // Determine controller
    if(isset($url[0])){

      $this->controller = $url[0].'Controller';

    }else{
        
      $this->controller = 'defaultController';
    
    }

    // Determine action
    if(isset($url[1])){

      $this->action = $url[1].'Action';

    }else{
        
      $this->action = 'indexAction';
    
    }

    // Set Params
    unset($url[0]);
    unset($url[1]);
  

    $this->params = $url;
  }

  public function run(){
    
    if (file_exists(CDIR."/".$this->controller.".php")) {
    
      require_once CDIR."/".$this->controller.".php";

      // Controller Check
      if (class_exists($this->controller)) {
       
        $controller = new $this->controller;
        
        //Method Check
        if (method_exists($controller, $this->action)) {
         
          call_user_func_array([$controller, $this->action], $this->params);

        } else {
          header("location:".URL);
          exit();

        }

      } else {

        header("location:".URL);
        exit();

      }

    } else {

      header("location:".URL);
      exit();

    }
  }
}