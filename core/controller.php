<?php

class controller{
  

  public function render($file, array $params = []){

    return view::render($file, $params);

  }

  public function model($model){

    if (file_exists(MDIR."/".$model.".php")) {

      require_once(MDIR."/".$model.".php");

      if (class_exists($model)) {
          
        return new $model;

      } else {

        exit("Class doesn't exist! ".$model);
      
    }

    } else {

      exit("Model file doesn't exits! ".$model.".php");
    
    }
  }


}