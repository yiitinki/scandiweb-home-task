<?php

class view{

  public static function render($view, array $params = []){

    if (file_exists(VDIR."/".$view.".php")) {

      extract($params);

      ob_start();

      require(VDIR."/".$view.".php");

      echo ob_get_clean();

    } else {

      exit("Görünüm dosyası bulunamadı:".VDIR."/".$view.".php");

    }
  }
  public static function assets($file){

    return URL."/public/".$file;

  }
}