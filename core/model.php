<?php
class model{

    public $db;

    public function __construct(){

        $this->db = new PDO(DB, DB_USER, DB_PASS);

    }

    public function query($query, array $params){

        $sth = $this->db->prepare($query);
        return $sth->execute($params);
    
    }

    
    public function fetch($query, array $params){

        $sth = $this->db->prepare($query,$params);
        $sth->execute($params);
    
        return $sth->fetchAll();
    
    }


    public function fetchAll($query){

        $sth = $this->db->prepare($query);
        $sth->execute();
        
        return $sth->fetchAll();
        
    }


    public function insert($table, array $params){

        $columns = array_keys($params);
        $columnLine = rtrim(implode(',',$columns),",");
        $valuesLine = ":".rtrim(implode(',:',$columns),",:");

        
        
        return $this->query("INSERT INTO ".$table." (".$columnLine.") VALUES (".$valuesLine.") ", $params);


    }

    public function remove($table,array $params){

        $statementCount = count($params);
        $columns = array_keys($params);
        $whereLine;


    
        if($statementCount > 1){

            $columnsArray = [];

            foreach($columns as $column){

                array_push($columnsArray,$column."=:".$column." AND ");

            }

            $whereLine = rtrim(implode('',$columns)," AND ");


        }else{

            $whereLine = $columns[0]."=:".$columns[0];

        }

        return $this->query("DELETE from ".$table." WHERE ".$whereLine,$params);

    }


   
}
