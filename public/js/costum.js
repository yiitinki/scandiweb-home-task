// Product delete 
$("#delete").click(function(){
    $("#success").hide();

    var val = [];
    
    $('.checkbox:checked').each(function(i){
        val[i] = $(this).val();
    });

    if(val.length > 0){
        $("#deleteBox").hide();
        $("#alertBox").show();

        $("input[type=checkbox]").prop( "disabled", true );
    }
   
    $("#deleteYes").click(function(){

        $.ajax({	
				type	: "POST",
				url:'/product/postDelete',
                data:{'id':val},
                
				success	:function(data){
                    
                    normalizePage();

                    $("#delete").prop("disabled", true );

                    if(data == 'success'){
                        val.forEach(element => {
                            $("#"+element).hide();
                        });
    
                        $("#success").show();
    
                    }else{
                        alert("There is an unexpected error");
                    }
                   

				},
				error:function(){
					alert("There is an unexpected error");
				}
		});

    });

       
    $("#deleteNo").click(function(){
        normalizePage();
    });

    function normalizePage(){

        $("#deleteBox").show();
        $("#alertBox").hide();

        $("input[type=checkbox]").prop( "disabled", false );

    }


});

// Active-deactive delete button
$(".checkbox").click(function(){

    var checkedBox = $('.checkbox:checked').length;

    if(checkedBox > 0){

        $("#delete").prop( "disabled", false );
    }else{
        $("#delete").prop( "disabled", true );
    }

    if(checkedBox > 1){
        $("#delete").html("Mass Delete");
    }else{
        $("#delete").html("Delete");
    }

});

// Show product description
$(document).on('change','#typeSwitcher',function(){
    var type = $("#typeSwitcher").val();
    
    $("#1,#2,#3").hide();
    $("#"+type).show();
});

// Create post
$("#submitPost").click(function(e){
    e.preventDefault();

    $("#error ul, #success ul").text("");
    $("#error, #success").hide();
    
    var sku = $("input[name='sku']").val();
    var name = $("input[name='name']").val();
    var price = $("input[name='price']").val();
    var type = $("#typeSwitcher option:selected").val();
    var size = $("input[name='size']").val();
    var weight = $("input[name='weight']").val();
    var height = $("input[name='height']").val();
    var widht = $("input[name='widht']").val();
    var lenght = $("input[name='lenght']").val();

    var description;
    var urlAction;

    switch(type){

        case "1":
            description = size;
        break;

        case "2":
            description = weight;
        break;

        case "3":
            description = height+"x"+widht+"x"+lenght;
        break;

    }

    $.ajax({	
        type	: "POST",
        url:"/product/postCreate",
        data:{
            'sku' : sku,
            'name' : name,
            'price' : price,
            'type' : type,
            'description' : description
        },
        
        success	:function(data){

            data = JSON.parse(data);

            $("#"+data.status).show();

            data.message.forEach(function(message){
                
                $("#"+data.status+" ul").append("<li>"+message+"</li>");
            
            });

            if(data.status == "success"){
                
                $("#productAddForm, h1").hide();
            
            }
    
        },
        error:function(){
            alert("There is an unexpected error");
        }
    });
});

