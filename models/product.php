<?php

class product extends model{

    protected $table = "products";
    
    public function getAll(){

        return $this->fetchAll("SELECT * FROM ".$this->table." INNER JOIN types ON ".$this->table.".type_id = types.type_id ");

    }

    public function getById($id){

        return $this->fetch("SELECT * FROM ".$this->table." WHERE id=:id",array(
            "id" => $id
        ));

    }


    public function getBySku($sku){

        return $this->fetch("SELECT * FROM ".$this->table." WHERE sku=:sku",array(
            "sku" => $sku
        ));

    }

    public function add(array $params){

        return $this->insert($this->table,$params);

    }

    public function delete(array $params){
        
        return $this->remove($this->table,$params);

    }

}