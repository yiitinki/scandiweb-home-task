<?php

use Particle\Validator\Validator;

class productController extends controller{
  
  public function indexAction(){
    header("location:".URL."/product/list");
  }

  public function listAction(){

    
    $product = $this->model('product');
    $query = $product->getAll();

    $this->render('product/header'); 
    $this->render('product/list',array('products' => $query)); 
    $this->render('product/footer');

    }

  public function createAction(){

    $type = $this->model('type');
    $query = $type->getAll();

    $this->render('product/header'); 
    $this->render('product/create', array("types" => $query)); 
    $this->render('product/footer');


  }

  public function postCreateAction(){
 

    $validator = new Validator;
    
    if($_SERVER['REQUEST_METHOD'] != "POST"){
      header("location:".URL);
      exit();
    }

    $name = $_POST["name"];
    $sku = $_POST["sku"];
    $price = $_POST["price"];
    $type = $_POST["type"];
    $description = $_POST["description"];
    $unit = ["1" => "MB", "2" => "KG", "3" => ""];

    if(is_array($description)){
      $description = implode("x",$description);
    }

    
    $data = array(
      'name' => $name,
      'sku' => $sku,
      'price' => $price,
      'type_id' => $type,
      'description' => $description
    );


    $validator = new Validator;

    $validator->required('name')->lengthBetween(2, 100)->alpha('Rule\Alnum::ALLOW_SPACES');
    $validator->required('sku')->lengthBetween(2, 100)->alnum();
    $validator->required('price')->lengthBetween(0, 100)->numeric()->greaterThan(0);
    $validator->required('type_id')->digits()->between(1,3);
    $validator->required('description')->lengthBetween(2, 100)->alnum();


    $result = $validator->validate($data);
    $skuStatus = $this->skuChecker($sku);

    if($result->isValid() AND $skuStatus){

      $data["description"] = $description." ".$unit[$type];
        
      $product = $this->model('product');

      $query = $product->add($data);

      if($query){

        $message = [
          'status' => 'success',
          'message' => ["Product has been created!"]
        ];
        echo json_encode($message);

      }else{
        
        $message = [
          'status' => 'error',
          'message' => ['There is an error during insert db, please check your db settings.']
        ];
        echo json_encode($message);

      }
    
    }else{

      $errorsCount = count($result->getMessages());
      $errors = [];

      if(!$skuStatus){
        array_push($errors,"SKU should be an unique!");
      }

      for($i=0;$i < $errorsCount; $i++){
        array_push($errors,array_values(array_values($result->getMessages())[$i])[0]);
      }


      $message = [
        'status' => 'error',
        'message' =>  $errors
      ];
  
      echo json_encode($message);

    }

  }



  public function postDeleteAction(){

    if($_SERVER['REQUEST_METHOD'] != "POST"){
      header("location:".URL);
      exit();
    }

    $productID = $_POST["id"];
    $deleteAction = [];

    foreach($productID as $id){

      if(!is_numeric($id) ){

        header("location:".URL);
        exit();
        break;

      }

      array_push($deleteAction,$this->delete($id));

    }

    if(in_array(false,$deleteAction)){
      echo "error";
    }else{
      echo "success";
    }

  }

  private function delete($id){

    $product = $this->model('product');
    $query = $product->delete(array(
      "product_id" => $id
    ));

    if($query){
      return true;

    }else{
      return false;
    }
    
    

  }


  public function skuChecker($sku){

    $product = $this->model("product");
    $query = $product->getBySku($sku);

    if(count($query) == 0){
      return true;
    }else{
      return false;
    }

  }
  


}